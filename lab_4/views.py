from django.http import response
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http.response import HttpResponseRedirect

def index(request):
    notes = Note.objects.all()
    response = {'notes':notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
    # create object of form
    form = NoteForm(request.POST or None)
    # check if form data is valid
    if form.is_valid() and request.method == 'POST':
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-4')

    context['form'] = form
    return render(request, 'lab4_form.html', context)

def note_list(request):
    notes = Note.objects.all()
    response = {'notes':notes}
    return render(request, 'lab4_note_list.html', response)