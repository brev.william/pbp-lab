### Apakah perbedaan antara JSON dan XML?

Baik JSON maupun XML, keduanya memiliki fungsi untuk melakukan transfer data API. Walaupun demikian, keduanya juga memiliki perbedaan sebagai berikut.

- Keduanya bukan bahasa pemrograman. XML adalah suatu _markup language_, sementara JSON adalah format penulisan data yang mengikuti format JavaScript.
- XML menyimpan data dalam bentuk struktur _tree_, sementara JSON menyimpan data dalam bentuk pasangan _key_ dan _value_.
- Untuk menyimpan data yang sama, _file_ XML akan memiliki ukuran yang lebih besar dan kecepatan pemrosesan yang lebih lambat dibandingkan JSON. Hal ini disebabkan oleh _tree structure_ yang dimiliki XML.
- JSON hanya dapat meng-_handle_ tipe data primitif, sementara XML mampu meng-_handle_ tipe data yang lebih kompleks, seperti gambar.
- _File_ XML diakhiri dengan `.xml`, sementara JSON diakhiri dengan `.json`.

### Apakah perbedaan antara HTML dan XML?
Meskipun keduanya adalah _markup language_, HTML dan XML memiliki beberapa perbedaan sebagai berikut.

- Seperti yang sudah dijelaskan pada jawaban soal sebelumnya, XML merupakan bahasa yang digunakan untuk transfer data. Sedangkan HTML digunakan untuk menampilkan data pada web.
- Semua _tag_ pada HTML bersifat _predefined_. Sebaliknya, _tag_ pada XML bersifat fleksibel karena pemrogram dapat membuatnya sendiri seturut dengan keinginannya.
- Dalam penulisan _tag_, XML bersifat _case sensitive_ sementara HTML tidak demikian.
- Pada XML, setiap _tag_ haruslah memiliki _closing tag_. HTML tidak lah demikian. Contohnya adalah _tag_ `<br>`.
- XML mendukung penggunaan _namespaces_, sementara HTML tidak demikian.

### Referensi
https://status200.net/xml-vs-json/  
https://www.guru99.com/xml-vs-html-difference.html
