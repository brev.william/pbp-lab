from django.contrib import admin
from .models import Dummy, Note

# Register your models here.
admin.site.register(Note)
admin.site.register(Dummy)