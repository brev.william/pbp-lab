import 'package:flutter/material.dart';
import './pallete.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    theme: ThemeData(
      primarySwatch: Palette.kToDark,
      accentColor: Colors.indigo,
      canvasColor: Color.fromRGBO(23, 30, 36, 1),
      fontFamily: 'Raleway',
      textTheme: ThemeData.light().textTheme.copyWith(
          bodyText1: TextStyle(
            color: Colors.white,
          ),
          bodyText2: TextStyle(
            color: Colors.white,
          ),
          headline6: TextStyle(
            fontSize: 20,
            fontFamily: 'RobotoCondensed',
            fontWeight: FontWeight.bold,
          )),
    ),
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  String title = "Tidak ada judul";
  String date = "Tidak ada tanggal";
  String time = "Tidak ada jam";
  String note = "Tidak ada note";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Reminder"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    onSaved: (String? value) {
                      title = value!;
                    },
                    decoration: new InputDecoration(
                      hintText: "ex: Berburu Pokemon",
                      labelText: "Title",
                      icon: Icon(Icons.bookmark),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Title tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    onSaved: (String? value) {
                      date = value!;
                    },
                    decoration: new InputDecoration(
                      labelText: "Date",
                      icon: Icon(Icons.calendar_today),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Date tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    onSaved: (String? value) {
                      time = value!;
                    },
                    decoration: new InputDecoration(
                      labelText: "Time",
                      icon: Icon(Icons.timer),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Time tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    onSaved: (String? value) {
                      note = value!;
                    },
                    decoration: new InputDecoration(
                      labelText: "Note",
                      icon: Icon(Icons.note_add),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Note tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Color(0xff1c2a35),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      return;
                    }
                    _formKey.currentState!.save();

                    print(title);
                    print(date);
                    print(time);
                    print(note);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
