import 'package:flutter/material.dart';

class Category {
  final String id;
  final String title;
  final Color color;
  final String date;
  final String time;

  const Category({
    @required this.id,
    @required this.title,
    @required this.date,
    @required this.time,
    this.color = Colors.orange,
  });
}
