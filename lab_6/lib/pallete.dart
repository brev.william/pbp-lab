//palette.dart
import 'package:flutter/material.dart';

class Palette {
  static const MaterialColor kToDark = const MaterialColor(
    0xff1c2a35, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    const <int, Color>{
      50: const Color(0xff192630), //10%
      100: const Color(0xff16222a), //20%
      200: const Color(0xff141d25), //30%
      300: const Color(0xff111920), //40%
      400: const Color(0xff0e151b), //50%
      500: const Color(0xff0b1115), //60%
      600: const Color(0xff080d10), //70%
      700: const Color(0xff06080b), //80%
      800: const Color(0xff030405), //90%
      900: const Color(0xff000000), //100%
    },
  );
} // you can define define int 500 as the default shade and add your lighter tints above and darker tints below.
